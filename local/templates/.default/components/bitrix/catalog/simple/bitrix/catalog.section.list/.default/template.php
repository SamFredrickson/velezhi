<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
	preg_match('/[^\/catalog\/][a-zA-Z\W\S]+[^\/]/',
	$APPLICATION->GetCurDir(), $code);
	$section_id = 1;

	if(!empty($code[0]) && !isset($_GET['PAGEN_1']))
	{
		$id = CIBlockSection::GetList(array(), array('IBLOCK_ID' => $arParams["IBLOCK_ID"], 'CODE' => $code[0]));
		$section_id = $id->Fetch();
		$section_id = $section_id['ID'];
	}

?>

<li class="<?= $section_id === 1  ? 'active' : ''?>" data-tab="0"> <a href="/catalog"> Все напитки </a> </li>

<?
foreach($arResult["SECTIONS"] as $arSection):
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	?>
<li class="<?= $arSection['ID'] === $section_id ? 'active' : ''?>" data-tab="<?=$arSection['ID']?>">
	<a href="<?=$arSection['LIST_PAGE_URL'].$arSection['CODE'].'/'?>">
		<?=$arSection["NAME"]?>
    </a>
</li>
<?
endforeach;
?>
