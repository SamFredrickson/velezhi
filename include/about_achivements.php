<div class="section-title title-line">
    <h1>
        Наши
        <span>достижения</span>
    </h1>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="achiev-item convex-border">
			<div class="achiev-item-wrap convex-border">
				<div class="achiev-icon">
					<img src="<?=DEFAULT_TEMPLATE_PATH?>/img/achiev-icon1.png" alt="Иконка">
				</div>
				<p>
					Фестиваль напитков <strong>«АлтайФест» 2016</strong>. Номинация «Лучший напиток года-2016» 
					Специальная награда за развитие производства безалкогольных напитков на 
					местном сырье
				</p>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="achiev-item convex-border">
			<div class="achiev-item-wrap convex-border">
				<div class="achiev-icon">
					<img src="<?=DEFAULT_TEMPLATE_PATH?>/img/achiev-icon2.png" alt="Иконка">
				</div>
				<p>
					Гранпри на выставке<br>
					в <strong>Кузбасе</strong>. Золотые медали <br>
					во всех номинациях
				</p>
			</div>
		</div>
	</div>
</div>