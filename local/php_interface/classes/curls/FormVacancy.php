<?

class FormVacancy {
    public function checkFormVacancy($fio, $phone, $mail, $date_birth, $address, $citizenship, $foreign_language,
        $post, $graphic_mode, $education, $experience)
    {
        if( empty($fio) || empty($phone) || empty($mail) || empty($date_birth) || empty($address) || empty($citizenship)
            || empty($foreign_language) || empty($post) || empty($graphic_mode)){
            return 0;
        }

        for($i = 0; $i < count($education); $i++)
        {
            if(empty($education[$i]['start_year']) || empty($education[$i]['end_year'])
                || empty($education[$i]['name_institution']) || empty($education[$i]['specialty']))
            {
                return 0;
            }
        }

        for($i = 0; $i < count($experience); $i++)
        {
            if(empty($experience[$i]['work_period']) || empty($experience[$i]['name_company'])
                || empty($experience[$i]['company_post']) || empty($experience[$i]['reason_dismissal']))
            {
                return 0;
            }
        }
        return 1;
    }

    public function checkCheckBox($checkbox)
    {
        if( !$checkbox ) {
            return 0;
        }
        return 1;
    }

    public function createMessageEducation($education)
    {
        $result = '';
        for ($i = 0; $i < count($education); $i++)
        {
            $result = $result."Год поступления: ".$education[$i]['start_year']
                ."<br>Год окончания: ".$education[$i]['end_year']
                ."<br>Наименование заведения: ".$education[$i]['name_institution']
                ."<br>Специальность: ".$education[$i]['specialty']."<br>";
            $result = $result."<br>";
        }
        return $result;
    }

    public function createMessageExperience($experience)
    {
        $result = '';
        for ($i = 0; $i < count($experience); $i++)
        {
            $result = $result."Период работы:".$experience[$i]['work_period']
                ."<br>Название компании: ".$experience[$i]['name_company']
                ."<br>Должность: ".$experience[$i]['company_post']
                ."<br>Причина увольнения: ".$experience[$i]['reason_dismissal']."<br>";
            $result = $result."<br>";
        }
        return $result;
    }
}

?>