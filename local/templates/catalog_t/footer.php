<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

</main>

<footer class="footer convex-border" id="footer">
        <div class="container">
        <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footer_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "footer_menu",
		"DELAY" => "N",
		"MAX_LEVEL" => "2",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "footer_menu",
		"USE_EXT" => "Y",
		"COMPONENT_TEMPLATE" => "footer_menu"
	),
	false
);?>
            <div class="row align-items-center">
                <div class="col-lg-3">
                    <a href="/" class="footer-logo logo">
                        <svg width="228" height="70" viewBox="10 0 228 70" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g filter="url(#logo)">
                            <path d="M10.5371 58.1761H11.9556V61.7912H16.9254V58.1761H18.3439V66.9912H16.9254V63.112H11.9606V66.9912H10.5371V58.1761Z" fill="#B9653E"/>
                            <path d="M24.5647 58C26.2689 58 27.2441 58.8805 27.584 59.3404L27.7416 59.6045H27.7613V58.1761H29.1798V66.9911H27.7613V65.5823H27.7416L27.584 65.8269C27.2638 66.3014 26.2689 67.1672 24.5647 67.1672C22.2399 67.1672 20.3042 65.2301 20.3042 62.5836C20.3042 59.942 22.2399 58 24.5647 58ZM24.742 65.8464C26.4462 65.8464 27.7613 64.5061 27.7613 62.5836C27.7613 60.6611 26.4462 59.3208 24.742 59.3208C23.0378 59.3208 21.7227 60.6611 21.7227 62.5836C21.7227 64.5061 23.0378 65.8464 24.742 65.8464Z" fill="#B9653E"/>
                            <path d="M31.667 58.1761H39.39V66.9912H37.9715V59.4969H33.0904V66.9912H31.6719V58.1761H31.667Z" fill="#B9653E"/>
                            <path d="M41.8774 58.1761H43.296V64.7898L43.2763 64.966H43.3107L48.0884 58.1761H49.6842V66.9912H48.2657V60.3775L48.2854 60.2013H48.2509L43.4733 66.9912H41.8774V58.1761Z" fill="#B9653E"/>
                            <path d="M51.019 58.1761H58.6534V59.4969H55.5455V66.9912H54.127V59.4969H51.019V58.1761Z" fill="#B9653E"/>
                            <path d="M59.9834 58.1761H61.4019V61.7031H63.1751L66.017 58.1761H67.7015L64.4163 62.3195L67.9675 66.9912H66.1943L63.1751 63.0239H61.4019V66.9912H59.9834V58.1761Z" fill="#B9653E"/>
                            <path d="M69.3071 58.1761H70.7256V64.7898L70.7059 64.966H70.7404L75.5181 58.1761H77.1139V66.9912H75.6954V60.3775L75.7151 60.2013H75.6806L70.903 66.9912H69.3071V58.1761Z" fill="#B9653E"/>
                            <path d="M84.5757 58.1761H92.2987V66.9912H90.8802V59.4969H85.9991V66.9912H84.5806V58.1761H84.5757Z" fill="#B9653E"/>
                            <path d="M98.8695 58C101.534 58 103.485 59.9372 103.485 62.5836C103.485 65.2301 101.529 67.1672 98.8695 67.1672C96.2098 67.1672 94.2544 65.2301 94.2544 62.5836C94.2495 59.942 96.2049 58 98.8695 58ZM98.8695 65.8464C100.717 65.8464 102.066 64.5061 102.066 62.5836C102.066 60.6611 100.717 59.3208 98.8695 59.3208C97.0225 59.3208 95.6729 60.6611 95.6729 62.5836C95.6729 64.5061 97.0225 65.8464 98.8695 65.8464Z" fill="#B9653E"/>
                            <path d="M105.258 61.439H111.651V62.8478H105.258V61.439Z" fill="#B9653E"/>
                            <path d="M113.961 58.1761H115.379V61.7912H120.349V58.1761H121.768V66.9912H120.349V63.112H115.379V66.9912H113.961V58.1761Z" fill="#B9653E"/>
                            <path d="M127.984 58C129.688 58 130.663 58.8805 131.003 59.3404L131.16 59.6045H131.18V58.1761H132.599V66.9911H131.18V65.5823H131.165L131.008 65.8269C130.688 66.3014 129.693 67.1672 127.989 67.1672C125.664 67.1672 123.728 65.2301 123.728 62.5836C123.723 59.942 125.659 58 127.984 58ZM128.161 65.8464C129.865 65.8464 131.18 64.5061 131.18 62.5836C131.18 60.6611 129.865 59.3208 128.161 59.3208C126.457 59.3208 125.142 60.6611 125.142 62.5836C125.147 64.5061 126.457 65.8464 128.161 65.8464Z" fill="#B9653E"/>
                            <path d="M138.992 58C140.765 58 141.834 58.7925 142.454 59.5507C142.828 60.0105 143.094 60.5193 143.252 61.0867H141.833C141.691 60.7492 141.499 60.4704 141.247 60.2062C140.824 59.7659 140.11 59.3257 138.992 59.3257C137.287 59.3257 135.972 60.666 135.972 62.5885C135.972 64.5452 137.376 65.8513 138.992 65.8513C140.144 65.8513 140.873 65.3768 141.316 64.8828C141.582 64.599 141.779 64.2811 141.917 63.9142H143.336C143.178 64.5306 142.892 65.0784 142.518 65.5529C141.863 66.3454 140.814 67.177 138.987 67.177C136.499 67.177 134.549 65.2399 134.549 62.5934C134.554 59.942 136.509 58 138.992 58Z" fill="#B9653E"/>
                            <path d="M143.966 58.1761H151.601V59.4969H148.493V66.9912H147.074V59.4969H143.966V58.1761Z" fill="#B9653E"/>
                            <path d="M157.014 58C159.679 58 161.629 59.9372 161.629 62.5836C161.629 65.2301 159.674 67.1672 157.014 67.1672C154.349 67.1672 152.399 65.2301 152.399 62.5836C152.399 59.942 154.349 58 157.014 58ZM157.014 65.8464C158.861 65.8464 160.211 64.5061 160.211 62.5836C160.211 60.6611 158.861 59.3208 157.014 59.3208C155.167 59.3208 153.817 60.6611 153.817 62.5836C153.817 64.5061 155.167 65.8464 157.014 65.8464Z" fill="#B9653E"/>
                            <path d="M165.003 63.8213C164.668 63.6794 164.382 63.4886 164.116 63.2587C163.673 62.8527 163.229 62.2168 163.229 61.1797C163.229 59.4871 164.559 58.181 166.672 58.181H170.687V66.996H169.268V64.1735H166.426L164.865 66.996H163.229L165.003 63.8213ZM166.692 62.8478H169.268V59.4969H166.692C165.342 59.4969 164.648 60.1867 164.648 61.1699C164.648 62.1532 165.342 62.8478 166.692 62.8478Z" fill="#B9653E"/>
                            <path d="M173.169 58.1761H174.588V65.6704H178.139V58.1761H179.557V65.6704H183.108V58.1761H184.527V65.6704H185.768V69.0213H184.35V66.9961H173.164V58.1761H173.169Z" fill="#B9653E"/>
                            <path d="M191.28 58C193.856 58 195.806 59.9372 195.806 62.4956V63.0239H188.172C188.369 64.6626 189.645 65.8464 191.28 65.8464C192.344 65.8464 193.053 65.46 193.536 65.054C193.802 64.8241 194.033 64.5599 194.21 64.2615H195.629C195.417 64.8094 195.112 65.3035 194.723 65.7242C194.028 66.4481 192.964 67.1721 191.28 67.1721C188.704 67.1721 186.753 65.235 186.753 62.5885C186.753 59.942 188.704 58 191.28 58ZM194.299 61.7911C194.102 60.4704 192.984 59.3208 191.28 59.3208C189.576 59.3208 188.438 60.4655 188.206 61.7911H194.299Z" fill="#B9653E"/>
                            <path d="M197.762 58.1761H199.894L202.559 63.6403H203.002L205.667 58.1761H207.8V66.9912H206.381V59.9372L206.401 59.7611H206.366L203.721 65.2252H201.86L199.215 59.7611H199.18L199.2 59.9372V66.9912H197.781V58.1761H197.762Z" fill="#B9653E"/>
                            <path d="M210.898 58.1761L212.853 65.6704H214.183L216.493 58.1761H218L214.272 70.342H212.765L213.828 66.9912H211.784L209.386 58.1761H210.898Z" fill="#B9653E"/>
                            <path d="M34.5433 26.7147C33.6075 26.2255 32.5682 25.8439 31.4157 25.6776V25.1346C32.3515 24.8607 33.174 24.4791 33.8882 23.9361C35.1491 23.0116 36.3608 21.4805 36.3608 18.8633C36.3608 15.3168 33.1199 12.0491 26.4804 12.0491H10V40.4019H26.4706C34.1591 40.4019 37.7251 36.8554 37.7251 32.2228C37.7251 29.1704 36.1342 27.5903 34.5433 26.7147ZM19.3336 18.3204H24.8255C26.4706 18.3204 27.298 19.0297 27.298 20.5021C27.298 21.9745 26.4755 22.6838 24.8255 22.6838H19.3336V18.3204ZM25.3722 34.1306H19.3336V28.6812H25.3722C27.2931 28.6812 28.3915 29.7182 28.3915 31.4059C28.3915 33.0985 27.2931 34.1306 25.3722 34.1306Z" fill="#B9653E"/>
                            <path d="M56.3922 11.2322C47.6052 11.2322 40.7441 18.1051 40.7441 26.2255C40.7441 34.3508 47.6052 41.2189 56.3922 41.2189C62.6524 41.2189 66.6025 38.6556 68.7993 36.0385C70.1193 34.566 71.1044 32.8197 71.7644 30.8581H61.884C61.6082 31.4597 61.2782 31.9489 60.7856 32.3843C59.9631 33.2012 58.5889 33.8567 56.3922 33.8567C53.0971 33.8567 50.9545 31.7288 50.3536 27.8594H72.0402V26.2255C72.0402 18.1003 65.3958 11.2322 56.3922 11.2322ZM50.6294 23.2269C51.2353 20.7712 53.3187 18.5943 56.3922 18.5943C59.4656 18.5943 61.554 20.7761 62.1549 23.2269H50.6294Z" fill="#B9653E"/>
                            <path d="M122.004 11.2322C113.217 11.2322 106.356 18.1051 106.356 26.2255C106.356 34.3508 113.217 41.2189 122.004 41.2189C128.264 41.2189 132.214 38.6556 134.411 36.0385C135.731 34.566 136.716 32.8197 137.376 30.8581H127.496C127.22 31.4597 126.89 31.9489 126.397 32.3843C125.575 33.2012 124.201 33.8567 122.004 33.8567C118.709 33.8567 116.566 31.7288 115.965 27.8594H137.652V26.2255C137.652 18.1003 131.008 11.2322 122.004 11.2322ZM116.236 23.2269C116.842 20.7712 118.926 18.5943 121.999 18.5943C125.073 18.5943 127.161 20.7761 127.762 23.2269H116.236Z" fill="#B9653E"/>
                            <path d="M208.203 12.0491L198.047 27.8642V23.5008V12.0491H188.713V40.4019H198.318L208.479 24.5916V28.9502V40.4019H217.813V12.0491H208.203Z" fill="#B9653E"/>
                            <path d="M178.803 11.3447L167.731 17.6942V5H158.048V17.6942L146.98 11.3447L142.139 19.6754L153.206 26.0249L142.139 32.3696L146.98 40.7003L158.048 34.3508V47.045H167.731V34.3508L178.803 40.7003L183.645 32.3696L172.578 26.0249L183.645 19.6754L178.803 11.3447Z" fill="#B9653E"/>
                            <path d="M80.2315 12.0491L73.0503 40.6416L82.448 42.9701L89.2992 15.6788L96.1505 42.9701L105.548 40.6416L98.3669 12.0491H80.2315Z" fill="#B9653E"/>
                            </g>
                            <defs>
                            <filter id="logo" x="0" y="0" width="228" height="85.342" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                            <feOffset dy="5"/>
                            <feGaussianBlur stdDeviation="5"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0.295833 0 0 0 0 0.227668 0 0 0 0 0.198455 0 0 0 0.4 0"/>
                            <feBlend mode="multiply" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                            </filter>
                            </defs>
                        </svg>                            
                    </a>
                </div>
                <div class="col-lg-6">
                    <ul class="footer-social">
                        <li>
                            <a href="#">
                                <svg width="13" height="27" viewBox="0 0 13 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12.1815 8.92651L11.7225 13.5244H8.15675V26.5322H2.87873V13.5244H0.363281V8.92651H2.87873V5.95459C2.87873 3.82275 3.86725 0.498535 8.2097 0.498535L12.1197 0.516602V4.979C12.1197 4.979 9.74545 4.979 9.27766 4.979C8.81871 4.979 8.15675 5.21387 8.15675 6.22559V8.92651H12.1815Z" fill="url(#paint0_linear)"/>
                                    <defs>
                                    <linearGradient id="paint0_linear" x1="6.27237" y1="0.498535" x2="6.27237" y2="26.5322" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="white" stop-opacity="0.75"/>
                                    <stop offset="1" stop-color="white"/>
                                    </linearGradient>
                                    </defs>
                                </svg>                                                                      
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M20.8356 0.681641H4.07223C2.18635 0.681641 0.635742 2.22878 0.635742 4.11043V19.6654C0.635742 22.1743 2.64735 24.265 5.16185 24.265V24.3486H19.6622C22.2605 24.3486 24.2721 22.2579 24.2721 19.749V4.11043C24.2721 2.22878 22.7215 0.681641 20.8356 0.681641ZM17.4829 3.4414H21.5061V7.37196L17.4829 7.45559V3.4414ZM8.34689 12.4733C8.34689 11.5534 8.59834 10.7171 9.10124 10.0481C9.8556 8.96091 11.029 8.29187 12.4539 8.29187C13.795 8.29187 15.0522 9.04454 15.8066 10.0481C16.2257 10.7171 16.5609 11.5534 16.5609 12.4733C16.5609 14.8149 14.6332 16.6548 12.3701 16.6548C10.107 16.6548 8.17926 14.8149 8.34689 12.4733ZM21.9252 19.749C21.9252 21.0035 20.9194 22.007 19.746 22.007H5.24567C3.98842 22.007 2.98261 21.0035 2.98261 19.749V10.0481H6.50292C6.16765 10.8007 6.00002 11.637 6.00002 12.4733C6.00002 15.9857 8.84979 18.9128 12.4539 18.9128C15.9742 18.9128 18.9078 15.9857 18.9078 12.4733C18.9078 11.637 18.7402 10.8007 18.4049 10.0481H21.9252V19.749Z" fill="url(#paint0_linear)"/>
                                    <defs>
                                    <linearGradient id="paint0_linear" x1="12.4539" y1="0.681641" x2="12.4539" y2="24.3486" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="white" stop-opacity="0.75"/>
                                    <stop offset="1" stop-color="white"/>
                                    </linearGradient>
                                    </defs>
                                </svg>                                    
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="27" height="18" viewBox="0 0 27 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M25.7391 2.90895C25.4164 1.55535 24.2823 0.553685 22.927 0.409301C19.7001 0.0483398 16.4362 0.0483398 13.1909 0.0483398C9.93624 0.0483398 6.67242 0.0483398 3.44547 0.409301C2.09015 0.553685 0.956108 1.55535 0.633413 2.90895C0.181641 4.84009 0.181641 6.93367 0.181641 8.92797C0.181641 10.9133 0.181641 13.0159 0.633413 14.938C0.946888 16.2916 2.08093 17.2932 3.44547 17.4466C6.67242 17.7986 9.92702 17.7986 13.1816 17.7986C16.4362 17.7986 19.7001 17.7986 22.9178 17.4466C24.2823 17.2932 25.4072 16.2916 25.7299 14.938C26.1816 13.0159 26.1816 10.9133 26.1816 8.92797C26.1816 6.93367 26.1908 4.84009 25.7391 2.90895ZM9.25398 14.3965V4.65059C12.4348 6.28394 15.6065 7.89924 18.8242 9.54161C15.6157 11.1659 12.444 12.7722 9.25398 14.3965Z" fill="url(#paint0_linear)"/>
                                    <defs>
                                    <linearGradient id="paint0_linear" x1="13.1816" y1="0.0483398" x2="13.1816" y2="17.7986" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="white" stop-opacity="0.75"/>
                                    <stop offset="1" stop-color="white"/>
                                    </linearGradient>
                                    </defs>
                                    </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <svg width="28" height="16" viewBox="0 0 28 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M27.4533 14.0663C27.8281 14.7743 27.4949 15.5489 26.8535 15.5988H22.8966C21.872 15.6821 21.0556 15.2657 20.3725 14.566C19.8227 14.008 19.3229 13.4167 18.7898 12.842C18.5732 12.6004 18.3483 12.3839 18.0817 12.209C17.6152 11.9008 16.9904 12.084 16.7322 12.5838C16.4406 13.1418 16.374 13.7581 16.3407 14.3745C16.299 15.3073 16.0158 15.5489 15.0828 15.5988C13.0835 15.6905 11.1842 15.3823 9.41818 14.3745C7.86041 13.4916 6.66085 12.2339 5.6029 10.8097C3.56198 8.03616 1.99588 4.99609 0.588064 1.86441C0.271513 1.15645 0.504761 0.781644 1.27948 0.764986C2.57067 0.748328 3.8702 0.748328 5.15307 0.764986C5.65288 0.773315 5.99443 1.04817 6.20268 1.51459C6.93575 3.17205 7.70214 4.82118 8.76009 6.29541C9.07664 6.74517 9.38486 7.21159 9.85135 7.52809C10.2429 7.79462 10.6761 7.66968 10.8677 7.24491C11.0176 6.91175 11.0759 6.56193 11.1009 6.21212C11.2092 4.99609 11.2175 3.79672 11.0343 2.58903C10.9343 1.93104 10.4178 1.38133 9.75972 1.26472C9.39319 1.19809 9.4515 1.0565 9.62644 0.839947C9.95132 0.465144 10.2512 0.231934 10.851 0.231934H15.316C16.0241 0.373526 16.1824 0.690026 16.274 1.39799L16.2823 6.36204C16.274 6.64522 16.4156 7.45313 16.9154 7.63637C17.307 7.7613 17.5652 7.45313 17.8068 7.20326C18.9564 5.98723 19.7477 4.54633 20.4642 3.05545C20.7224 2.51406 20.9223 1.95603 21.1556 1.40631C21.3472 0.956552 21.6387 0.748328 22.1552 0.756657H26.462C26.5869 0.756657 26.7202 0.756657 26.8452 0.781644C27.5699 0.906578 27.7698 1.21475 27.5449 1.92271C27.1867 3.03879 26.5036 3.9633 25.8289 4.88781C25.1125 5.88729 24.3461 6.84512 23.6297 7.84459C22.9799 8.76077 23.0299 9.21887 23.8463 10.0101C24.7376 10.893 25.6956 11.7175 26.5036 12.692C26.8618 13.1168 27.2034 13.5582 27.4533 14.0663Z" fill="url(#paint0_linear)"/>
                                    <defs>
                                    <linearGradient id="paint0_linear" x1="14.0418" y1="0.231934" x2="14.0418" y2="15.6142" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="white" stop-opacity="0.75"/>
                                    <stop offset="1" stop-color="white"/>
                                    </linearGradient>
                                    </defs>
                                </svg>                                    
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3">
                    <a href="https://alianscompany.ru/" class="footer-company">
                        <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/alians.png" alt="Альянс">
                    </a>
                </div>
            </div>
        </div>
    </footer>
	<div id="product-modal" class="modal-js">

		<div class="modal__body">

			<form class="form convex-border">

				<div class="section-title">
					<h1>
						Запросить <span>цену</span>
						<button type="button" class="modal__close close-modal">
							<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19.8182 1.93934L1.93957 19.818M19.8182 19.818L1.93957 1.93934" stroke="#E8591A"
									  stroke-width="3" stroke-linecap="round"/>
							</svg>

						</button>
					</h1>
					<p>
                        Оставьте свои контактные данные и наш менеджер свяжется с Вами
                    </p>
				</div>
				<div class="form-wrap row">

					<div class="col-12 input-wrap">
						<textarea class="input" name="message" placeholder="Сообщение"></textarea>
					</div>
					<div class="col-lg-4 input-wrap">
						<input class="input" type="email" name="email" placeholder="Email">
					</div>
					<div class="col-lg-4 input-wrap">
						<input class="input" type="text" name="name" placeholder="Имя">
					</div>
					<div class="col-lg-4 input-wrap">
						<input class="input" type="tel" name="phone" placeholder="Телефон" maxlength="17" autocomplete="off">
					</div>
				</div>
				<div class="form-submit">
					<div class="form-check">
						<div class="checkbox">
							<input type="checkbox" name="checkbox">
							<label for="" class="checkbox"></label>
						</div>
						<p>
							<a href="/info/usloviya-obrabotki-personalnykh-dannykh/" class="link-agreement">
								Согласен c условиями обработки персональных данных
							</a>
						</p>
					</div>
					<button type="submit" class="btn btn-brown">
						<div class="btn-icon">
							<svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M22 9.00049L0.999999 9.00049M22 9.00049L15 17.0005M22 9.00049L15 1.00049"
									  stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
							</svg>
						</div>
						<span>Отправить заявку</span>
					</button>
				</div>
				<div class="message">
					<span>123</span>
				</div>
			</form>
		</div>
	</div>
	<div id="review-modal" class="modal-js">

		<div class="modal__body">

			<form class="form convex-border">

				<div class="section-title">
					<h1>
						Оставить <span>отзыв</span>
						<button type="button" class="modal__close close-modal">
							<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19.8182 1.93934L1.93957 19.818M19.8182 19.818L1.93957 1.93934" stroke="#E8591A"
									  stroke-width="3" stroke-linecap="round"/>
							</svg>

						</button>
					</h1>
					<p>
                        Поделитесь вашим мнением о нашей продукции и сотрудничестве
                    </p>
				</div>
				<div class="form-wrap row">

					<div class="col-12 input-wrap">
						<textarea class="input" name="message" placeholder="Сообщение"></textarea>
					</div>
					<div class="col-lg-4 input-wrap">
						<input class="input" type="text" name="name" placeholder="Имя">
					</div>
					<div class="col-lg-4 input-wrap">
						<input class="input" type="tel" name="phone" placeholder="Телефон" maxlength="17" autocomplete="off">
					</div>
					<input type="hidden" name="review" value="review">
				</div>
				<div class="form-submit">
					<div class="form-check">
						<div class="checkbox">
							<input type="checkbox" name="checkbox">
							<label for="" class="checkbox"></label>
						</div>
						<p>
							<a href="/info/usloviya-obrabotki-personalnykh-dannykh/" class="link-agreement">
								Согласен c условиями обработки персональных данных
							</a>
						</p>
					</div>
					<button type="submit" class="btn btn-brown">
						<div class="btn-icon">
							<svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M22 9.00049L0.999999 9.00049M22 9.00049L15 17.0005M22 9.00049L15 1.00049"
									  stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
							</svg>
						</div>
						<span>Отправить заявку</span>
					</button>
				</div>
				<div class="message">
					<span>123</span>
				</div>
			</form>
		</div>
	</div>

	<div id="vacancy-modal" class="modal-js vacancy-modal">

		<div class="modal__body">

			<form class="form convex-border" enctype="multipart/form-data">
				<div class="section-title">
					<h1>
						Хочу здесь <span>работать</span>
						<button type="button" class="modal__close close-modal">
							<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M19.8182 1.93934L1.93957 19.818M19.8182 19.818L1.93957 1.93934" stroke="#E8591A"
									  stroke-width="3" stroke-linecap="round"/>
							</svg>

						</button>
					</h1>
                    <input type="hidden" name="vacancy" value="vacancy">
					<div class="summary-form">
						<input name="file_cv" type="file">

						<p>
							<span class="icon-summary">
								<svg width="24" height="24" viewBox="0 0 24 24" fill="none"
									 xmlns="http://www.w3.org/2000/svg">
								<g filter="url(#filter0_d)">
								<path d="M20.5 14L14 20.5C11.5147 22.9853 7.48528 22.9853 5 20.5V20.5C2.51472 18.0147 2.51472 13.9853 5 11.5L13.25 3.25C15.0449 1.45507 17.9551 1.45508 19.75 3.25V3.25C21.5449 5.04492 21.5449 7.95507 19.75 9.75L12 17.5C10.8954 18.6046 9.10457 18.6046 8 17.5V17.5C6.89543 16.3954 6.89543 14.6046 8 13.5L14 7.5"
									  stroke="url(#paint0_linear)" stroke-width="2" stroke-linecap="round"
									  stroke-linejoin="round"/>
								</g>
								<defs>
								<filter id="filter0_d" x="-1.86426" y="-1.09619" width="27.9602" height="30.4602"
										filterUnits="userSpaceOnUse"
										color-interpolation-filters="sRGB">
								<feFlood flood-opacity="0" result="BackgroundImageFix"/>
								<feColorMatrix in="SourceAlpha" type="matrix"
											   values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
								<feOffset dy="2"/>
								<feGaussianBlur stdDeviation="2"/>
								<feColorMatrix type="matrix"
											   values="0 0 0 0 0.241667 0 0 0 0 0.192479 0 0 0 0 0.163125 0 0 0 0.2 0"/>
								<feBlend mode="multiply" in2="BackgroundImageFix" result="effect1_dropShadow"/>
								<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
								</filter>
								<linearGradient id="paint0_linear" x1="11.75" y1="0" x2="11.75" y2="25"
												gradientUnits="userSpaceOnUse">
								<stop stop-color="white" stop-opacity="0.6"/>
								<stop offset="1" stop-color="white"/>
								</linearGradient>
								</defs>
								</svg>

						</span>
							<span>Прикрепить резюме</span>
						</p>
					</div>
				</div>
				<div class="form-wrap row">
					<div class="col-12 input-wrap">
						<input class="input" type="text" name="fio" placeholder="ФИО">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="tel" name="phone" placeholder="Телефон" maxlength="17" autocomplete="off">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="email" name="mail" placeholder="Email">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input date-birth-input" name="date_birth" type="text" placeholder="Дата рождения">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="text" name="address" placeholder="Адресс проживания">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="text" name="citizenship" placeholder="Гражданство">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="text" name="foreign_language" placeholder="Иностранный язык (какой, степень владения">
					</div>
				</div>


				<div class="form-block-title">
					<h2>Требования к месту работы</h2>
				</div>

				<div class="form-wrap row">
					<div class="col-lg-6 col-md-6 input-wrap">
						<input class="input" type="text" name="post" placeholder="Должность">
					</div>
					<div class="col-lg-6 col-md-6 input-wrap">
						<input class="input" type="text" name="graphic_mode" placeholder="Желаемый график и режим работы">
					</div>
				</div>

				<div class="form-block-title">
					<h2>Образование</h2>
				</div>

				<div class="form-wrap row">
					<div class="col-lg-2 col-md-6 input-wrap">
						<input class="input date-input" type="text" name="education[0][start_year]" placeholder="Год поступления">
					</div>
					<div class="col-lg-2 col-md-6 input-wrap">
						<input class="input date-input" type="text" name="education[0][end_year]" placeholder="Год окончания">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="text" name="education[0][name_institution]" placeholder="Наименование учебного заведения">
					</div>
					<div class="col-lg-4 col-md-6 input-wrap">
						<input class="input" type="text" name="education[0][specialty]" placeholder="Специальность">
					</div>

					<div class="form-add-btn form-add-btn-js" data-dop-form="#dopEducation">
						<span>+</span><span>Добавить еще одно учебное заведение</span>
					</div>
				</div>

				<div class="form-block-title">
					<h2>Опыт работы</h2>
				</div>

				<div class="form-wrap row justify-content-between">
					<div class="col-lg-2 col-md-6 input-wrap">
						<input class="input date-period" type="text" name="experience[0][work_period]" placeholder="Период работы">
					</div>
					<div class="col-lg-3 col-md-6 input-wrap">
						<input class="input" type="text" name="experience[0][name_company]" placeholder="Название компании">
					</div>
					<div class="col-lg-3 col-md-6 input-wrap">
						<input class="input" type="text" name="experience[0][company_post]" placeholder="Должность">
					</div>
					<div class="col-lg-3 col-md-6 input-wrap">
						<input class="input" type="text" name="experience[0][reason_dismissal]" placeholder="Причина увольнения">
					</div>
					<div class="form-add-btn form-add-btn-js" data-dop-form="#workExperience">
						<span>+</span><span>Добавить еще одно место работы</span>
					</div>
				</div>


				<div class="form-submit">
					<div class="form-check">
						<div class="checkbox">
							<input type="checkbox" name="checkbox" id="">
							<label for="" class="checkbox"></label>
						</div>
						<p>
							<a href="/info/usloviya-obrabotki-personalnykh-dannykh/" class="link-agreement">
								Согласен c условиями обработки персональных данных
							</a>
						</p>
					</div>


					<button type="submit" class="btn btn-brown">
						<div class="btn-icon">
							<svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M22 9.00049L0.999999 9.00049M22 9.00049L15 17.0005M22 9.00049L15 1.00049"
									  stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
							</svg>
						</div>
						<span>Отправить заявку</span>
					</button>
				</div>
				<div class="message">
					<span>123</span>
				</div>
			</form>
		</div>
	</div>


	<template id="dopEducation">
		<div class="col-lg-2 col-md-6 input-wrap">
			<input class="input date-input" type="text" placeholder="Год поступления">
		</div>
		<div class="col-lg-2 col-md-6 input-wrap">
			<input class="input date-input" type="text" placeholder="Год окончания">
		</div>
		<div class="col-lg-4 col-md-6 input-wrap">
			<input class="input" type="text" placeholder="Наименование учебного заведения">
		</div>
		<div class="col-lg-4 col-md-6 input-wrap">
			<input class="input" type="text" placeholder="Специальность">
		</div>
	</template>


	<template id="workExperience">
		<div class="col-lg-2 col-md-6 input-wrap">
			<input class="input date-period" type="text" placeholder="Период работы">
		</div>
		<div class="col-lg-3 col-md-6 input-wrap">
			<input class="input" type="text" placeholder="Название компании">
		</div>
		<div class="col-lg-3 col-md-6 input-wrap">
			<input class="input" type="text" placeholder="Должность">
		</div>
		<div class="col-lg-3 col-md-6 input-wrap">
			<input class="input" type="text" placeholder="Причина увольнения">
		</div>
	</template>


	<div class="btn btn-brown scroll-top">
		<div class="btn-icon">
			<svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M22 9.00049L0.999999 9.00049M22 9.00049L15 17.0005M22 9.00049L15 1.00049" stroke="white"
					  stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
			</svg>
		</div>
	</div>
	<script src="<?=DEFAULT_TEMPLATE_PATH?>/js/recentArrow.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/modal.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="<?=DEFAULT_TEMPLATE_PATH?>/js/slick.min.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/rellax.min.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/main.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/custom.js"></script>
    <script src="<?=DEFAULT_TEMPLATE_PATH?>/js/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('input[type="tel"]').mask("+7(999)-999-99-99");
			$('input[type="tel"]').focus('click', function() {
				$(this).attr('placeholder', '+7(___)-___-__-__');
			});
			$('input[type="tel"]').blur('click', function() {
				$(this).attr('placeholder', 'Телефон');
			});

		})
	</script>
	<script>
		function functionName(){
			let str = document.querySelectorAll('.team-info h2');
			if(str.length !== 0){
				str.forEach(i=>{
					i.style.minWidth = 330 + 'px';
					i.innerHTML = i.innerHTML.substring(0, i.innerHTML.indexOf(' ')) + '<br>' + i.innerHTML.substring(i.innerHTML.indexOf(' '), i.innerHTML.length);
				})
			}
		}
		functionName()
	</script>
	<script>
	function maskDate() {
        	$(".date-input").mask("9999");
        	$(".date-birth-input").mask("99.99.9999");
        	$(".date-period").mask("9999-9999");
    	}
	maskDate();
	</script>


	<script>
    	$(document).ready(function () {
        	$('.scroll-top').click(function () {
            		$('body,html').animate({
                		scrollTop: 0
            		}, 500);
            		return false;
        	});
    	});
</script>
</body>
</html>