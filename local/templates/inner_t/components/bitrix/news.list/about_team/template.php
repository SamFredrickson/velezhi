<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
	<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="col-xl-6">
        <div class="team-item convex-border">
            <div class="team-img convex-border">
				<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="Фото">
            </div>
            <div class="team-info">
                <h2><?=$arItem['NAME']?></h2>
                <p><?=$arItem['PROPERTIES']['POSITION']['VALUE']?></p>
                <a href="tel:<?=$arItem['PROPERTIES']['PHONE']['VALUE']?>">
					<?=$arItem['PROPERTIES']['PHONE']['VALUE']?>
				</a>
			</div>
		</div>
    </div>
	<? endforeach;?>
</div>