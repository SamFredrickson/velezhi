<?php

class Poster
{
   private $sendsmsurl = "https://smsc.ru/sys/send.php?";

   public function sendSMS($login, $password, $phone, $message)
   {
        $params = [
            'login'     => $login,
            'psw'       => $password,
            'phones'    => $phone,
            'mes'       => $message
        ];

        $c = curl_init($this->sendsmsurl . http_build_query($params));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $response = curl_exec($c);
        curl_close($this->sendsmsurl);
       
   }

    public function sendMailPartner($email, $fio, $message, $phone)
    {
        $arEventFields = array(
            "FIO"   => $fio,
            "PHONE" => $phone,
            "EMAIL_FROM"    => $email,
            "MESSAGE"    => $message
        );
        CEvent::SendImmediate("BECOME_PARTNER", "s1", $arEventFields, "N", 34);
    }

    public function sendMailProduct($email, $fio, $message, $phone, $product_name)
    {
        $arEventFields = array(
            "FIO"   => $fio,
            "PHONE" => $phone,
            "EMAIL_FROM"    => $email,
            "MESSAGE"    => $message,
            "PRODUCT_NAME" => $product_name
        );
        CEvent::SendImmediate("REQUEST_PRODUCT_PRICE", "s1", $arEventFields, "N", 35);
    }

    public function sendMailReview($fio, $message, $phone)
    {
        $arEventFields = array(
            "FIO"   => $fio,
            "PHONE" => $phone,
            "MESSAGE"    => $message,
        );
        CEvent::SendImmediate("GIVE_FEEDBACK", "s1", $arEventFields, "N", 33);
    }

   public function sendMailVacancy($fio, $phone, $email, $date_birth, $address, $citizenship, $foreign_language,
                            $post, $graphic_mode, $education, $experience)
   {
       $arEventFields = array(
           "FIO"   => $fio,
           "PHONE" => $phone,
           "EMAIL_FROM"    => $email,
           "DATE_BIRTH"    => $date_birth,
           "ADDRESS"    => $address,
           "CITIZENSHIP"    => $citizenship,
           "FOREIGN_LANGUAGE"    => $foreign_language,
           "POST"    => $post,
           "OPERATING_MODE"    => $graphic_mode,
           "EDUCATION" => $education,
           "WORK_EXPERIENCE" => $experience
       );
        CEvent::SendImmediate("VACANCY_APPLICATION", "s1", $arEventFields, "N", 32);
   }


    public function sendMailVacancyWithFile($fio, $phone, $email, $date_birth, $address, $citizenship, $foreign_language,
                                    $post, $graphic_mode, $education, $experience, $file)
    {

        $preparedFile[] = CFIle::SaveFile($file, "mailatt");

        $arEventFields = array(
            "FIO"   => $fio,
            "PHONE" => $phone,
            "EMAIL_FROM"    => $email,
            "DATE_BIRTH"    => $date_birth,
            "ADDRESS"    => $address,
            "CITIZENSHIP"    => $citizenship,
            "FOREIGN_LANGUAGE"    => $foreign_language,
            "POST"    => $post,
            "OPERATING_MODE"    => $graphic_mode,
            "EDUCATION" => $education,
            "WORK_EXPERIENCE" => $experience,
        );
        CEvent::SendImmediate("VACANCY_APPLICATION", "s1", $arEventFields, "N", 32, $files = $preparedFile);

        CFile::Delete($preparedFile[0]);
    }
}