<?

class FormReview {
    public function checkFormReview($name, $msg, $phone)
    {
        if( empty($phone) || empty($name) || empty($msg)){
            return 0;
        }
        return 1;
    }

    public function checkCheckBox($checkbox)
    {
        if( !$checkbox ) {
            return 0;
        }
        return 1;
    }

    public function createMessage($name, $msg, $phone)
    {
        $result = "Имя: " . $name . "\n" . "Телефон: " . $phone . "\n" . "Сообщение: " . $msg . "\n";
        return $result;
    }
}

?>