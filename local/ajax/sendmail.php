<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>


<?

$poster = new Poster();

//$headers =	'MIME-Version: 1.0' . "\r\n" .
//'Content-type: text/html; charset=UTF-8' . "\r\n" .
// 'From: svbond@al-dev.ru' . "\r\n" .
// 'Reply-To: svbond@al-dev.ru' . "\r\n" .
//'X-Mailer: PHP/' . phpversion() . "\r\n";

$message = '';

if(isset($_POST['partners']))
{
    $email  = trim( htmlentities( $_POST['email'] ) );
    $name   = trim( htmlentities( $_POST['name'] ) );
    $msg    = trim( htmlentities( $_POST['message'] ) );
    $phone  = trim( htmlentities( $_POST['phone'] ) );
    $checkbox = $_POST['checkbox'] == 'on' ? 1 : 0;

    $formPartner = new FormPartner();

    if( !$formPartner->checkFormPartners($email, $name, $msg, $phone) )
    {
        echo json_encode(['status' => 500, 'error' => "Все поля обязательны для заполнения!"]); die();
    }
    if( !$formPartner->checkCheckBox($checkbox))
    {
        echo json_encode(['status' => 500, 'error' => "Вы не согласны с условиями обработки персональных данных!"]); die();
    }
    $message = $formPartner->createMessage($email, $name, $msg, $phone);

    $poster->sendMailPartner($email, $name, $msg, $phone);
    $poster->sendSMS('mr-alex7777@yandex.ru', 'AlexeY1971', 'G1', $message);
}

if(isset($_POST['product_name']))
{
    $email  = trim( htmlentities( $_POST['email'] ) );
    $name   = trim( htmlentities( $_POST['name'] ) );
    $msg    = trim( htmlentities( $_POST['message'] ) );
    $phone  = trim( htmlentities( $_POST['phone'] ) );
    $checkbox = $_POST['checkbox'] == 'on' ? 1 : 0;

    $formProduct = new FormProduct();

    if( !$formProduct->checkFormProduct($email, $name, $msg, $phone) )
    {
        echo json_encode(['status' => 500, 'error' => "Все поля обязательны для заполнения!"]); die();
    }

    if( !$formProduct->checkCheckBox($checkbox))
    {
        echo json_encode(['status' => 500, 'error' => "Вы не согласны с условиями обработки персональных данных!"]); die();
    }
    $message = $formProduct->createMessage($email, $name, $msg, $phone, $_POST['product_name']);

    $poster->sendMailProduct($email, $name, $msg, $phone, $_POST['product_name']);
    $poster->sendSMS('mr-alex7777@yandex.ru', 'AlexeY1971', 'G1', $message);
}

if(isset($_POST['review']))
{
    $name   = trim( htmlentities( $_POST['name'] ) );
    $msg    = trim( htmlentities( $_POST['message'] ) );
    $phone  = trim( htmlentities( $_POST['phone'] ) );
    $checkbox = $_POST['checkbox'] == 'on' ? 1 : 0;

    $formReview = new FormReview();

    if( !$formReview->checkFormReview($name, $msg, $phone) )
    {
        echo json_encode(['status' => 500, 'error' => "Все поля обязательны для заполнения!"]); die();
    }

    if( !$formReview->checkCheckBox($checkbox))
    {
        echo json_encode(['status' => 500, 'error' => "Вы не согласны с условиями обработки персональных данных!"]); die();
    }

    $message = $formReview->createMessage($name, $msg, $phone);

    $poster->sendMailReview($name, $msg, $phone);
    $poster->sendSMS('mr-alex7777@yandex.ru', 'AlexeY1971', 'G1', $message);
}

if(isset($_POST['vacancy']))
{
    $fio = trim( htmlentities( $_POST['fio'] ) );
    $phone = trim( htmlentities( $_POST['phone'] ) );
    $email = trim( htmlentities( $_POST['mail'] ) );
    $date_birth = trim( htmlentities( $_POST['date_birth'] ) );
    $address = trim( htmlentities( $_POST['address'] ) );
    $citizenship = trim( htmlentities( $_POST['citizenship'] ) );
    $foreign_language = trim( htmlentities( $_POST['foreign_language'] ) );
    $post = trim( htmlentities( $_POST['post'] ) );
    $graphic_mode = trim( htmlentities( $_POST['graphic_mode'] ) );

    for ($i = 0; $i < count($_POST['education']); $i++)
    {
        $_POST['education'][$i]['start_year'] = trim( htmlentities( $_POST['education'][$i]['start_year'] ) );
        $_POST['education'][$i]['end_year'] = trim( htmlentities( $_POST['education'][$i]['end_year'] ) );
        $_POST['education'][$i]['name_institution'] = trim( htmlentities( $_POST['education'][$i]['name_institution'] ) );
        $_POST['education'][$i]['specialty'] = trim( htmlentities( $_POST['education'][$i]['specialty'] ) );
    }

    for ($i = 0; $i < count($_POST['experience']); $i++)
    {
        $_POST['experience'][$i]['work_period'] = trim( htmlentities( $_POST['experience'][$i]['work_period'] ) );
        $_POST['experience'][$i]['name_company'] = trim( htmlentities( $_POST['experience'][$i]['name_company'] ) );
        $_POST['experience'][$i]['company_post'] = trim( htmlentities( $_POST['experience'][$i]['company_post'] ) );
        $_POST['experience'][$i]['reason_dismissal'] = trim( htmlentities( $_POST['experience'][$i]['reason_dismissal'] ) );
    }

    $checkbox = $_POST['checkbox'] == 'on' ? 1 : 0;

    $formVacancy = new FormVacancy();

    if(!$formVacancy->checkFormVacancy($fio, $phone, $email, $date_birth, $address, $citizenship,
        $foreign_language, $post, $graphic_mode, $_POST['education'], $_POST['experience']))
    {
        echo json_encode(['status' => 500, 'error' => "Все поля обязательны для заполнения!"]); die();
    }

    if( !$formVacancy->checkCheckBox($checkbox))
    {
        echo json_encode(['status' => 500, 'error' => "Вы не согласны с условиями обработки персональных данных!"]); die();
    }

    $education = $formVacancy->createMessageEducation($_POST['education']);
    $experience = $formVacancy->createMessageExperience($_POST['experience']);

    if(!empty($_FILES['file_cv']['name']))
    {
        $poster->sendMailVacancyWithFile($fio, $phone, $email, $date_birth, $address, $citizenship, $foreign_language,
            $post, $graphic_mode, $education, $experience, $_FILES['file_cv']);
    }
    else{
        $poster->sendMailVacancy($fio, $phone, $email, $date_birth, $address, $citizenship, $foreign_language,
            $post, $graphic_mode, $education, $experience);
    }
}

echo json_encode(['status' => 200, 'success' => "Сообщение успешно отправлено!"]);