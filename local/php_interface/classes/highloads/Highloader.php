<?php

use Bitrix\Main\Loader; 
Loader::includeModule("highloadblock"); 

use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

class Highloader
{
    public static function GetHighloader($id, $field, $mode){
      
        $hlbl = $id;
        $values = [];
        $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch(); 

        $entity = HL\HighloadBlockTable::compileEntity($hlblock); 
        $entity_data_class = $entity->getDataClass(); 

        $rsData = $entity_data_class::getList(array(
             "select" => array("*"),
        ));

        while($arData = $rsData->Fetch()){
           if( $mode ) array_push($values, $arData[$field]);
           else array_push($values, CFile::GetPath($arData[$field]));
        }

        return $values;
    }
}