<div class="section-title title-line">
                    <h1>
                        Команда
                    </h1>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="team-item convex-border">
                            <div class="team-img convex-border">
                                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/team1.png" alt="Фото Алексея">
                            </div>
                            <div class="team-info">
                                <h2>Алексей Камышов</h2>
                                <p>Руководитель</p>
                                <a href="tel:+79609597777">
                                    +7(960) 959-77-77
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="team-item convex-border">
                            <div class="team-img convex-border">
                                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/team2.png" alt="Фото Екатерины">
                            </div>
                            <div class="team-info">
                                <h2>Волкова Екатерина</h2>
                                <p>Оператор, Прием Заявок</p>
                                <a href="tel:+78007008167">
                                    +7(800) 700-81-67
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="team-item convex-border">
                            <div class="team-img convex-border">
                                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/team3.png" alt="Фото Ивана">
                            </div>
                            <div class="team-info">
                                <h2>Иван Камышов</h2>
                                <p>Торговый представитель</p>
                                <a href="tel:+79635246699">
                                    +7(963) 524-66-99
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="team-item convex-border">
                            <div class="team-img convex-border">
                                <img src="<?=DEFAULT_TEMPLATE_PATH?>/img/team4.png" alt="Фото Левана">
                            </div>
                            <div class="team-info">
                                <h2>Леван Дедабришвили</h2>
                                <p>Торговый представитель</p>
                                <a href="tel:+79635246699">
                                    +7(963) 524-66-99
                                </a>
                            </div>
                        </div>
                    </div>
                </div>