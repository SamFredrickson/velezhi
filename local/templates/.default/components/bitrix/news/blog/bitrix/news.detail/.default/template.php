<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="review">
    <div class="container" id="blog">
        <div class="row">
            <div class="col-md-12">
				<?if(!empty($arResult["PROPERTIES"]['GALLERY']['VALUE'])):?>
				<div class="testimonials-slider">
					<a href="#" class="arrow arrow-left nav-icon">
						<div>
							<svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M2 6.5L15 6.5M2 6.5L6.33333 2M2 6.5L6.33333 11" stroke="url(#paint0_radial)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
								<defs>
								<radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.5 2) rotate(90) scale(9 13)">
								<stop stop-color="#C7724A"/>
								<stop offset="1" stop-color="#B2633E"/>
								</radialGradient>
								</defs>
							</svg> 
						</div>                            
					</a>
					<div class="testimonials-slider-wrap">  
						<?foreach($arResult["PROPERTIES"]['GALLERY']['VALUE'] as $image):?>
						<div class="testimonials-slide">
							<?$file = CFile::ResizeImageGet($arResult["PROPERTIES"]['GALLERY']['VALUE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true); ?>
							<img src="<?=CFile::GetPath($image)?>" alt="Галерея">
						</div>
						<?endforeach;?>
					</div>
					<a href="#" class="arrow arrow-right nav-icon">
						<div>
							<svg width="17" height="13" viewBox="0 0 17 13" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M15 6.5L2 6.5M15 6.5L10.6667 2M15 6.5L10.6667 11" stroke="url(#paint0_radial)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
								<defs>
								<radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(8.5 2) rotate(90) scale(9 13)">
								<stop stop-color="#C7724A"/>
								<stop offset="1" stop-color="#B2633E"/>
								</radialGradient>
								</defs>
							</svg>  
						</div>                                                     
					</a>
				</div>
				<?else:?>			
				<img class="col-md-5" style="margin-bottom: 10px;" align="left" src="<?=$arResult["DETAIL_PICTURE"]['SRC']?>" alt="">
				<?endif;?>
				<p class="review__name"> <?=$arResult['NAME']?> | <?=$arResult['DATE_ACTIVE_FROM']?></p>
				<p class="review__text text-justify"> <?=$arResult['DETAIL_TEXT']?> </p>
				<p class="review__next"> <a href="/blog/"> Вернуться... </a> </p>
			</div>
		</div>
	</div>
</section>