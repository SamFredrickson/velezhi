<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;


//delayed function must return a string
if(empty($arResult))
	return "";


	$strReturn = '<div class="container"><ul class="breadcrumbs">';

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
$css = $APPLICATION->GetCSSArray();
if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
{
	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
}

$strReturn .= '<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$main = [
	"TITLE" => htmlspecialcharsex($arResult[0]["TITLE"]),
	"LINK" => htmlspecialcharsex($arResult[0]["LINK"])
];


$strReturn .= '<li class="">
<a href="' . $main['LINK'] . '">
<svg width="12" height="13" viewBox="0 0 12 13" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M12 11.4V6.30293C12 5.73501 12 5.45105 11.9275 5.18909C11.8632 4.95703 11.7576 4.73849 11.6156 4.54397C11.4554 4.3244 11.2329 4.14801 10.7878 3.79523L10.7878 3.79522L7.98782 1.57571C7.27788 1.01295 6.92291 0.731577 6.52981 0.623584C6.18303 0.528316 5.81697 0.528316 5.47019 0.623584C5.07709 0.731577 4.72212 1.01295 4.01219 1.57571L1.21218 3.79522L1.21218 3.79523C0.767129 4.14801 0.544604 4.3244 0.384385 4.54397C0.24245 4.73849 0.136808 4.95703 0.0725435 5.18909C0 5.45105 0 5.73501 0 6.30293V11.4C0 11.9601 0 12.2401 0.108993 12.454C0.204867 12.6422 0.357847 12.7951 0.546009 12.891C0.759921 13 1.03995 13 1.6 13H2.18947C2.74953 13 3.02955 13 3.24346 12.891C3.43163 12.7951 3.58461 12.6422 3.68048 12.454C3.78947 12.2401 3.78947 11.9601 3.78947 11.4V9.20976C3.78947 8.6497 3.78947 8.36968 3.89847 8.15577C3.99434 7.9676 4.14732 7.81462 4.33548 7.71875C4.54939 7.60976 4.82942 7.60976 5.38947 7.60976H6.61053C7.17058 7.60976 7.4506 7.60976 7.66452 7.71875C7.85268 7.81462 8.00566 7.9676 8.10153 8.15577C8.21053 8.36968 8.21053 8.6497 8.21053 9.20976V11.4C8.21053 11.9601 8.21053 12.2401 8.31952 12.454C8.41539 12.6422 8.56837 12.7951 8.75653 12.891C8.97045 13 9.25047 13 9.81053 13H10.4C10.9601 13 11.2401 13 11.454 12.891C11.6422 12.7951 11.7951 12.6422 11.891 12.454C12 12.2401 12 11.9601 12 11.4Z" fill="url(#paint0_linear)"/>
  <defs>
  <linearGradient id="paint0_linear" x1="6" y1="0" x2="6" y2="13" gradientUnits="userSpaceOnUse">
  <stop stop-color="white" stop-opacity="0.6"/>
  <stop offset="1" stop-color="white"/>
  </linearGradient>
  </defs>
</svg>'.
$main['TITLE']. '
</a>
<svg width="5" height="8" viewBox="0 0 5 8" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M1 1L3.29289 3.29289C3.68342 3.68342 3.68342 4.31658 3.29289 4.70711L1 7" stroke="#8F837B" stroke-linecap="round"/>
</svg>
</li>';

$itemSize = count($arResult);
for($index = 1; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 1? '<i class="bx-breadcrumb-item-angle fa fa-angle-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .=  $arrow.'
			<div class="bx-breadcrumb-item" id="bx_breadcrumb_'.$index.'" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				<a class="bx-breadcrumb-item-link" href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">
					<span class="bx-breadcrumb-item-text" itemprop="name">'.$title.'</span>
				</a>
				<meta itemprop="position" content="'.($index + 1).'" />
			</div>';
	}
	else
	{
		$strReturn .= $arrow.'
			<div class="bx-breadcrumb-item">
				<span class="bx-breadcrumb-item-text">'.$title.'</span>
			</div>';
	}
}


$strReturn .= '</ul></div>';

return $strReturn;
