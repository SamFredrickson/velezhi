<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<form action="<?=POST_FORM_ACTION_URI?>" method="POST" class="form convex-border">
                        <div class="section-title">
                            <h1>
                                Хочу стать <span>партнером</span>
                            </h1>
                            <p>
                                Оставьте свои контактные данные и наш менеджер свяжется с Вами
                            </p>
                        </div>
                        <div class="form-wrap row">
                            <div class="col-lg-4 input-wrap">
                                <input class="input" type="email"  name="user_email"  placeholder="Email">
                            </div>
                            <div class="col-lg-4 input-wrap">
							<input class="input" type="text" name="user_name" placeholder="Имя">
                            </div>
                            <div class="col-lg-4 input-wrap">
                                <input class="input" type="tel" name="user_phone" placeholder="Телефон">
                            </div>
                            <div class="col-12 input-wrap">
                                <textarea class="input" name="MESSAGE" placeholder="Сообщение"></textarea>
                            </div>
                        </div>
                        <div class="form-submit">
                            <div class="form-check">
                                <div class="checkbox">
                                    <input type="checkbox" id="form352">
                                    <label for="form352" class="checkbox"></label>
                                </div>
                                <p>
                                    Согласен с условиями обработки персональных данных
                                </p>
                            </div>
                            <div class="btn btn-brown">
                                <div class="btn-icon">
                                    <svg width="23" height="18" viewBox="0 0 23 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22 9.00049L0.999999 9.00049M22 9.00049L15 17.0005M22 9.00049L15 1.00049" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>                                    
                                </div>
								<input type="hidden" name="PARAMS_HASH" value="<?=$arResult["PARAMS_HASH"]?>">
                                <input type="submit" name="submit" value="Отправить заявку">
                            </div>
                        </div>
                    </form>
