// Variables

const forms = document.querySelectorAll('.form');



// Email Logic

document.addEventListener("DOMContentLoaded", function(){
    if(forms.length > 0){
        for(let form of forms)
        {
            form.addEventListener('submit', (e) => {
                e.preventDefault();
                let message = e.target.querySelector('.message span');

                message.parentElement.classList.add("visible");
                message.classList.add("success");
                message.innerHTML = "Пожалуйста, подождите.";
                
                fetch('/local/ajax/sendmail.php', {
                    method: 'post',
                    body: new FormData(e.target)
    
                }).then(response => response.json())
                  .then(data =>	{
					if( data.status === 500 ){
						if(message.classList.contains("success"))
						  message.classList.remove("success");
						  
						  message.classList.add("error");
						  message.parentElement.classList.add("visible");
						  message.innerHTML = data.error;
						}
					if( data.status === 200 ){
						if(message.classList.contains("error"))
							message.classList.remove("error");
						
						message.classList.add("success");
						message.parentElement.classList.add("visible");
						message.innerHTML = data.success;
						
						e.target.reset();
                    }
                  });
            })
        }
    }
})

