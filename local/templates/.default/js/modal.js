var $jscomp = $jscomp || {};
$jscomp.scope = {};
$jscomp.createTemplateTagFirstArg = function (a) {
    return a.raw = a
};
$jscomp.createTemplateTagFirstArgWithRaw = function (a, b) {
    a.raw = b;
    return a
};
var modalLinks = document.querySelectorAll(".modal-link"), body = document.body,
    modalCloseBtns = document.querySelectorAll(".close-modal"), unlock = !0, timeout = 150;
0 < modalLinks.length && modalLinks.forEach(function (a) {
    return a.addEventListener("click", function (b) {
        var c = a.getAttribute("href") || a.getAttribute("data-modal");
        c = document.querySelector(c);
        a.getAttribute("data-product-name") ? openModalProduct(c, a.getAttribute("data-product-name")) : openModal(c);
        b.preventDefault()
    })
});
0 < modalCloseBtns.length && modalCloseBtns.forEach(function (a) {
    return a.addEventListener("click", function (b) {
        modalCLose(a.closest(".modal-js"));
        b.preventDefault()
    })
});

function openModalProduct(a, b) {
    if (a && unlock) {
        var c = document.querySelector(".modal-js.open");
        c ? modalCLose(c, !1) : bodyLock();
        a.classList.add("open");
        setTimeout(function () {
            autoFocus(a)
        }, 150);
        c = document.createElement("input");
        c.type = "hidden";
        c.setAttribute("name", "product_name");
        c.setAttribute("value", b);
        a.querySelector(".form").append(c);
        a.addEventListener("click", function (d) {
            d.target.closest(".modal__body") ? "" : modalCLose(d.target.closest(".modal-js"))
        })
    }
}

function openModal(a) {
    if (a && unlock) {
        var b = document.querySelector(".modal-js.open");
        b ? modalCLose(b, !1) : bodyLock();
        a.classList.add("open");
        let input = a.querySelector(".summary-form input[type='file']");
        input.addEventListener('change', readFile);
        setTimeout(function () {
            autoFocus(a)
        }, 150);
        a.addEventListener("click", function (c) {
            c.target.closest(".modal__body") ? "" : modalCLose(c.target.closest(".modal-js"))
        })
    }
}

function readFile() {
    let file = this.files[0];
    let reader = new FileReader();
    reader.readAsText(file);
    let infText = this.parentElement.querySelector('p span:not(.icon-summary)');
    let submitBtn = this.closest('form').querySelector(".btn[type='submit']");
    reader.onloadstart =()=>{
        infText.textContent = 'Началась загрузка';
        submitBtn.setAttribute("disabled", "disabled");
        submitBtn.style.opacity = .5;
    }
    reader.onload =()=> {
        infText.textContent = file.name;
        submitBtn.removeAttribute("disabled");
        submitBtn.style.opacity = 1;
    };
    reader.onerror =()=> {
        infText.textContent = 'Ошибка загрузки';
        submitBtn.removeAttribute("disabled");
        submitBtn.style.opacity = 1;
    };
    reader.onloadend = ()=>{
        submitBtn.removeAttribute("disabled");
        submitBtn.style.opacity = 1;
    }
}

function modalCLose(a, b) {
    b = void 0 === b ? !0 : b;
    unlock && (a.classList.remove("open"), b && bodyUnLock());
    a.querySelector("input[type='hidden']") && a.querySelector("input[type='hidden']").remove()
}

function bodyLock() {
    body.style.paddingRight = window.innerWidth - body.offsetWidth + "px";
    body.classList.add("modal-page");
    unlock = !1;
    setTimeout(function () {
        unlock = !0
    }, timeout)
}

function bodyUnLock() {
    setTimeout(function () {
        body.style.paddingRight = "0px";
        body.classList.remove("modal-page")
    }, timeout);
    setTimeout(function () {
        unlock = !0
    }, timeout)
}

document.addEventListener("keydown", function (a) {
    27 === a.which && (a = document.querySelector(".modal-js.open"), modalCLose(a))
});
var formAddBtns = document.querySelectorAll(".form-add-btn-js");
0 < formAddBtns.length && formAddBtns.forEach(function (a) {
    return a.addEventListener("click", function () {
        "#dopEducation" === a.dataset.dopForm && addDopEducation(a);
        "#workExperience" === a.dataset.dopForm && addDopWorkExperience(a);
        maskDate()
    })
});

function addDopEducation(a) {
    var b = document.querySelector(a.dataset.dopForm).content.cloneNode(!0),
        c = a.parentElement.querySelectorAll(".input-wrap").length / 4;
    b.querySelectorAll("input")[0].setAttribute("name", "education[" + c + "][year_admission]");
    b.querySelectorAll("input")[1].setAttribute("name", "education[" + c + "][year_ending]");
    b.querySelectorAll("input")[2].setAttribute("name", "education[" + c + "][name_institution]");
    b.querySelectorAll("input")[3].setAttribute("name", "education[" + c + "][specialty]");
    a.before(b)
}

function addDopWorkExperience(a) {
    var b = document.querySelector(a.dataset.dopForm).content.cloneNode(!0),
        c = a.parentElement.querySelectorAll(".input-wrap").length / 4;
    b.querySelectorAll("input")[0].setAttribute("name", "experience[" + c + "][work_period]");
    b.querySelectorAll("input")[1].setAttribute("name", "experience[" + c + "][name_company]");
    b.querySelectorAll("input")[2].setAttribute("name", "experience[" + c + "][company_post]");
    b.querySelectorAll("input")[3].setAttribute("name", "experience[" + c + "][reason_dismissal]");
    a.before(b)
}

function autoFocus(a) {
    a = a.querySelectorAll(".input");
    for (var b = 0; b < a.length; b++) if ("" === a[b].value) {
        a[b].focus();
        break
    }
};