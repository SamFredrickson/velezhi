                <div class="section-title title-line">
                    <h1>Вакансии</h1>
                    <p>
                        Nam libero tempore, cum soluta nobis est eligendi optio 
                        cumque nihil impedit quo minus id quod maxime placeat facere 
                        possimus, omnis voluptas assumenda est, omnis dolor repellendus.
                    </p>
                </div>
                <ul class="vacancy-list">
                    <li class="vacancy-item convex-border">
                        <div class="vacancy-title">
                            <h2>Технолог</h2>
                        </div>
                        <div class="vacancy-desc">
                            <p>
                                Nam libero tempore, cum soluta nobis est eligendi 
                                optio cumque nihil impedit quo minus id quod maxime 
                                placeat facere possimus, omnis voluptas assumenda est, 
                                omnis dolor repellendus. Nam libero tempore, cum soluta 
                                nobis est eligendi optio cumque nihil impedit quo minus 
                                id quod maxime placeat facere possimus, 
                                omnis voluptas assumenda est, omnis dolor repellendus.
                            </p>
                        </div>
                    </li>
                    <li class="vacancy-item convex-border">
                        <div class="vacancy-title">
                            <h2>Технолог</h2>
                        </div>
                        <div class="vacancy-desc">
                            <p>
                                Nam libero tempore, cum soluta nobis est eligendi 
                                optio cumque nihil impedit quo minus id quod maxime 
                                placeat facere possimus, omnis voluptas assumenda est, 
                                omnis dolor repellendus. Nam libero tempore, cum soluta 
                                nobis est eligendi optio cumque nihil impedit quo minus 
                                id quod maxime placeat facere possimus, 
                                omnis voluptas assumenda est, omnis dolor repellendus.
                            </p>
                        </div>
                    </li>
                    <li class="vacancy-item convex-border">
                        <div class="vacancy-title">
                            <h2>Технолог</h2>
                        </div>
                        <div class="vacancy-desc">
                            <p>
                                Nam libero tempore, cum soluta nobis est eligendi 
                                optio cumque nihil impedit quo minus id quod maxime 
                                placeat facere possimus, omnis voluptas assumenda est, 
                                omnis dolor repellendus. Nam libero tempore, cum soluta 
                                nobis est eligendi optio cumque nihil impedit quo minus 
                                id quod maxime placeat facere possimus, 
                                omnis voluptas assumenda est, omnis dolor repellendus.
                            </p>
                        </div>
                    </li>
                </ul>
                <a href="#" class="vacancy-btn btn btn-brown">
                    <div class="btn-icon">
                        <svg width="31" height="26" viewBox="0 0 31 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g filter="url(#filt3asd)">
                            <path d="M26 11L5 11M26 11L19 19M26 11L19 3" stroke="url(#paint0_line22ar)" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </g>
                            <defs>
                            <filter id="filt3asd" x="0" y="0" width="31" height="26" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                            <feOffset dy="2"/>
                            <feGaussianBlur stdDeviation="2"/>
                            <feColorMatrix type="matrix" values="0 0 0 0 0.241667 0 0 0 0 0.192479 0 0 0 0 0.163125 0 0 0 0.2 0"/>
                            <feBlend mode="multiply" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                            </filter>
                            <linearGradient id="paint0_line22ar" x1="2.57692" y1="10.1111" x2="28.4231" y2="10.1111" gradientUnits="userSpaceOnUse">
                            <stop stop-color="white" stop-opacity="0.6"/>
                            <stop offset="1" stop-color="white"/>
                            </linearGradient>
                            </defs>
                        </svg>                                    
                    </div>
                    <span>
                        Хочу работать у вас
                    </span>
                </a>