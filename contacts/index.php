<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<section class="section">
	<div class="container">
		<p class="review__text">
			+7(960) 959-77-77
			<br>
 			Камышов Алексей Анатольевич
			<br>
 			Руководитель
			<br>
 			<br>
 			+7(800) 700-81-67
			<br>
 			Волкова Екатерина
			<br>
 			Оператор Прием Заявок
			<br>
 			<br>
 			+7(963)524-66-99
			<br>
 			Камышов Иван
			<br>
 			Торговый представитель г. Новосибирск
			<br>
 			<br>
 			+7(960)959-90-00
			<br>
 			Леван Дедабришвили
			<br>
 			Торговый представитель г. Барнаул
			<br>
 			<br>
			<span>E-mail:&nbsp;<a href="mailto:info@mail.ru">info@mail.ru</a></span>
			<br>
		</p>
	</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>