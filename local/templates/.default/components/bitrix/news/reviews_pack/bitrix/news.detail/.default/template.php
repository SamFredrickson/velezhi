<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>	

<section class="review">
    <div class="container">
        <div class="row justify-content-center" id="review__pack">
         
		<div class="col-md-12">
               <p class="review__name"> <?=$arResult["NAME"]?> </p>
               <p class="review__text"> <?=$arResult["DETAIL_TEXT"]?> </p>
               <p class="review__next"> 
					   <a href="/catalog"> Перейти в каталог |</a>
					   <a href="/reviews"> Все отзывы </a> 
			   </p>
           </div>

</div>
	</div>
	
	<?=$arResult["NAV_STRING"]?>
</section>
