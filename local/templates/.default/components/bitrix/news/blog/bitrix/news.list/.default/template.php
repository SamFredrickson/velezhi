<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<? //debug($arResult); ?>

<section class="review" id="blog">
    <div class="container">
        <div class="row">
         
<?foreach($arResult["ITEMS"] as $arItem):?>
	<? //debug($arItem); ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

		<div class="col-md-2" style="margin-bottom: 10px;"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><img src="<?=$arItem["DETAIL_PICTURE"]['SRC']?>" alt=""></a></div>	
            <div class="col-md-10">
               <p class="review__name"> <?=$arItem['NAME']?> | <?=$arItem['DATE_ACTIVE_FROM']?></p>
               <p class="review__text"> <?=$arItem['PREVIEW_TEXT']?> </p>
            <p class="review__next"> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"> Читать далее... </a> </p>
        </div>

<?endforeach;?>

</div>
    </div>
</section>