                <div class="row">
                    <div class="col-lg-4">
                        <div class="benefits-item convex-border">
                            <div class="benefits-content convex-border">
                                <h3>Улучшает работу ЖКТ</h3>
                                <p>
                                    Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. 
                                    Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. 
                                    В то время некий безымянный печатник создал большую коллекцию
                                </p>
                                <div class="benefits-icon">
                                    <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.0958 0H13.6574V34H22.0958V0Z" fill="url(#paint0_radial)"/>
                                        <path d="M4.8513 5.10221L0.900759 11.97L30.8029 29.2986L34.7535 22.4309L4.8513 5.10221Z" fill="url(#paint1_radial)"/>
                                        <path d="M30.7972 5.0979L0.89502 22.4266L4.84556 29.2943L34.7477 11.9657L30.7972 5.0979Z" fill="url(#paint2_radial)"/>
                                        <defs>
                                        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        </defs>
                                    </svg>                                          
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="benefits-item convex-border">
                            <div class="benefits-content convex-border">
                                <h3>Утоляет жажду</h3>
                                <p>
                                    Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. 
                                    Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. 
                                    В то время некий безымянный печатник создал большую коллекцию
                                </p>
                                <div class="benefits-icon">
                                    <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.0958 0H13.6574V34H22.0958V0Z" fill="url(#paint0_radial)"/>
                                        <path d="M4.8513 5.10221L0.900759 11.97L30.8029 29.2986L34.7535 22.4309L4.8513 5.10221Z" fill="url(#paint1_radial)"/>
                                        <path d="M30.7972 5.0979L0.89502 22.4266L4.84556 29.2943L34.7477 11.9657L30.7972 5.0979Z" fill="url(#paint2_radial)"/>
                                        <defs>
                                        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        </defs>
                                    </svg>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="benefits-item convex-border">
                            <div class="benefits-content convex-border">
                                <h3>Полезен для кожи и волос</h3>
                                <p>
                                    Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. 
                                    Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. 
                                    В то время некий безымянный печатник создал большую коллекцию
                                </p>
                                <div class="benefits-icon">
                                    <svg width="35" height="34" viewBox="0 0 35 34" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22.0958 0H13.6574V34H22.0958V0Z" fill="url(#paint0_radial)"/>
                                        <path d="M4.8513 5.10221L0.900759 11.97L30.8029 29.2986L34.7535 22.4309L4.8513 5.10221Z" fill="url(#paint1_radial)"/>
                                        <path d="M30.7972 5.0979L0.89502 22.4266L4.84556 29.2943L34.7477 11.9657L30.7972 5.0979Z" fill="url(#paint2_radial)"/>
                                        <defs>
                                        <radialGradient id="paint0_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint1_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        <radialGradient id="paint2_radial" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(17.8242) rotate(90) scale(34 33.8584)">
                                        <stop stop-color="#C7724A"/>
                                        <stop offset="1" stop-color="#B2633E"/>
                                        </radialGradient>
                                        </defs>
                                    </svg>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>