<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 
<?
if(CModule::IncludeModule("iblock")) {
 
	$IBLOCK_ID = 1; // указываем инфоблок с элементами
   
	$arOrder = Array("SORT"=>"DESC");
	$arSelect = Array("ID", "NAME", "IBLOCK_ID", "SECTION_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "ACTIVE"=>"Y");
	//$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	$resSection = CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect, false);
   }

?>

 
 <ul class="menu header-menu">
			<li class="menu-item-has-children">
                            <a href="/catalog/">
                                Напитки
                                <div class="open-submenu">
                                    <svg width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M3.91173 5.24452C3.52582 5.90584 2.57025 5.90584 2.18434 5.24452L0.419884 2.22083C0.0308604 1.55418 0.511723 0.716828 1.28358 0.716828L4.81249 0.716828C5.58435 0.716828 6.06521 1.55418 5.67619 2.22084L3.91173 5.24452Z" fill="white"/>
                                    </svg>
                                </div>                               
                            </a>
                            <ul class="submenu">
                                <? while($ob = $resSection->GetNextElement()): ?>
									<? $arFields = $ob->GetFields(); ?>
									<li>
										<a href="<?=$arFields["SECTION_PAGE_URL"]?>" class="parent">
												<?=$arFields["NAME"]?>
											</a>
									</li>

								<? endwhile; ?>
                            </ul>
                        </li>

<?if (!empty($arResult)):?>

<?
$previousLevel = 0;

foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?php if ($arItem["IS_PARENT"]):?>

<?php if ($arItem["DEPTH_LEVEL"] == 1):?>
	
	<li class="menu-item-has-children">
                            <a href="<?=$arItem["LINK"]?>">
								<?=$arItem["TEXT"]?>
                                <div class="open-submenu">
                                    <svg width="6" height="6" viewBox="0 0 6 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M3.91173 5.24452C3.52582 5.90584 2.57025 5.90584 2.18434 5.24452L0.419884 2.22083C0.0308604 1.55418 0.511723 0.716828 1.28358 0.716828L4.81249 0.716828C5.58435 0.716828 6.06521 1.55418 5.67619 2.22084L3.91173 5.24452Z" fill="white"/>
                                    </svg>
                                </div>                               
                            </a>
							
							
	<ul class="submenu">
		<?php else: ?>
			<li>
			<a href="<?=$arItem["LINK"]?>" class="parent">
					<?=$arItem["TEXT"]?>
				</a>
			</li>
		<?php endif; ?>


		<?php else: ?>

<?php if ($arItem["DEPTH_LEVEL"] == 1):?>
	<li<?php if ($arItem["SELECTED"]) echo ' class="active"';?>>
		<a href="<?=$arItem["LINK"]?>">
			<?=$arItem["TEXT"]?>
		</a>
	</li>
<?else:?>
	<li<?php if ($arItem["SELECTED"]) echo ' class="active"';?>>
		<a href="<?=$arItem["LINK"]?>">
			<?=$arItem["TEXT"]?>
		</a>
	</li>
<?endif?>

<?php endif; ?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

<?endif?>