<?

\Bitrix\Main\Loader::registerAutoLoadClasses(
    null,
    array(
        "Highloader"                =>  "/local/php_interface/classes/highloads/Highloader.php",
        "Poster"                    =>  "/local/php_interface/classes/curls/Poster.php",
        "FormPartner"                    =>  "/local/php_interface/classes/curls/FormPartner.php",
        "FormProduct"                    =>  "/local/php_interface/classes/curls/FormProduct.php",
        "FormReview"                    =>  "/local/php_interface/classes/curls/FormReview.php",
        "FormVacancy"                    =>  "/local/php_interface/classes/curls/FormVacancy.php"
    )
);

use Highloader;

define("DEFAULT_TEMPLATE_PATH", "/local/templates/.default");

function debug($data)
{
    echo '<pre>' . print_r($data) . '</pre>';
}