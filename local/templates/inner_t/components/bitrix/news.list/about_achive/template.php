<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="col-lg-6">
		<div class="achiev-item convex-border">
			<div class="achiev-item-wrap convex-border">
				<div class="achiev-icon">
					<img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="Иконка">
				</div>
				<p>
					<?=$arItem['DETAIL_TEXT']?>
				</p>
			</div>
		</div>
	</div>
	<? endforeach;?>
</div>