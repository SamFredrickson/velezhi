$(document).ready(function() {

    // Menu

    $('.burger-menu').click(function() {
        $(this).toggleClass('open')
        $('.collapse-mobile').toggleClass('open')
    })

    $('.open-submenu').click(function(e) {
        e.preventDefault()
        $(this).toggleClass('open')
        var currentSubMenu = $(this).parent().parent().find('.submenu')
        currentSubMenu.toggleClass('open')
    })

    // Parallax

    if( $('.leaves').length ) {
        var leavesParallax = new Rellax('.leaves img', {
            speed: 2,
            center:true
        })
    }

    // Slider

    if( $('.partners-slider').length ) {
        $('.partners-slider-wrap').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    
        $('.partners-slider .arrow-left').click(function(e) {
            e.preventDefault()
            $('.partners-slider-wrap').slick('slickPrev')
        })
        $('.partners-slider .arrow-right').click(function(e) {
            e.preventDefault()
            $('.partners-slider-wrap').slick('slickNext')
        })
    }

    if( $('.recent-slider').length ) {
        $('.recent-slider-wrap').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })

        $('.recent-slider .arrow-left').click(function(e) {
            e.preventDefault()
            $('.recent-slider-wrap').slick('slickPrev')
        })
        $('.recent-slider .arrow-right').click(function(e) {
            e.preventDefault()
            $('.recent-slider-wrap').slick('slickNext')
        })
    }

    if( $('.product-slider').length ) {
        $('.product-slider-wrap').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false
        })

        $('.product-slider .arrow-left').click(function(e) {
            e.preventDefault()
            $('.product-slider-wrap').slick('slickPrev')
        })
        $('.product-slider .arrow-right').click(function(e) {
            e.preventDefault()
            $('.product-slider-wrap').slick('slickNext')
        })
    }

    if( $('.testimonials-slider').length ) {
        $('.testimonials-slider-wrap').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        })

        $('.testimonials-slider .arrow-left').click(function(e) {
            e.preventDefault()
            $('.testimonials-slider-wrap').slick('slickPrev')
        })
        $('.testimonials-slider .arrow-right').click(function(e) {
            e.preventDefault()
            $('.testimonials-slider-wrap').slick('slickNext')
        })
    }
    

})
