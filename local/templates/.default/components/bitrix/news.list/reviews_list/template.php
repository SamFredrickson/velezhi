<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="review">
    <div class="container">
        <div class="row justify-content-center" id="review__pack">
         
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

		   <div class="col-md-12">
               <p class="review__name"> <?=$arItem["NAME"]?> </p>
               <p class="review__text"> <? echo substr($arItem["DETAIL_TEXT"], 0, 230); ?> </p>
               <p class="review__next"> <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"> Читать далее... </a> </p>
           </div>

<?endforeach;?>

</div>
	</div>
	
	<?=$arResult["NAV_STRING"]?>
</section>
