<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<ul class="vacancy-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<li class="vacancy-item convex-border">
        	<div class="vacancy-title">
                	<h2><?=$arItem['NAME']?></h2>
		</div>
                <div class="vacancy-desc">
                	<p>
				<?=$arItem['DETAIL_TEXT']?>
                        </p>
		</div>
	</li>
<?endforeach;?>
</ul>
